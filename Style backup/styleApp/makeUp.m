//
//  makeUp.m
//  styleApp
//
//  Created by Mac Mini on 06/12/13.
//  Copyright (c) 2013 Mac Mini. All rights reserved.
//

#import "makeUp.h"

@interface makeUp ()

@end

@implementation makeUp

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = FALSE;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
   // self.navigationController.navigationItem.rightBarButtonItem = [UIButton buttonWithType:]
    scrlview.contentSize = CGSizeMake(320, 600);
    scrlview.showsVerticalScrollIndicator = FALSE;
    [scrlview setScrollEnabled:TRUE];
    scrlview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"makeUp_bg.png"]];
    
    
    UIImageView *rackImgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 130, 260, 60)];
    rackImgView1.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView1];
    
    UIImageView *rackImgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 260, 260, 60)];
    rackImgView2.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView2];
    
    UIImageView *rackImgView3 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 390, 260, 60)];
    rackImgView3.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView3];
    
    UIImageView *rackImgView4 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 520, 260, 60)];
    rackImgView4.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView4];
    
    //////////////////////////////////////////////////////////////////////////
    
    UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(160, 90, 150, 20)];
    lbl1.text = @"Face";
    lbl1.textAlignment = NSTextAlignmentCenter;
    lbl1.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl1];
    
    UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(160, 220, 150, 20)];
    lbl2.text = @"Lips";
    lbl2.textAlignment = NSTextAlignmentCenter;
    lbl2.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl2];
    
    UILabel *lbl3 = [[UILabel alloc] initWithFrame:CGRectMake(160, 350, 150, 20)];
    lbl3.text = @"Eyes";
    lbl3.textAlignment = NSTextAlignmentCenter;
    lbl3.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl3];
    
    UILabel *lbl4 = [[UILabel alloc] initWithFrame:CGRectMake(160, 480, 150, 20)];
    lbl4.text = @"nails";
    lbl4.textAlignment = NSTextAlignmentCenter;
    lbl4.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl4];
    
    //////////////////////////////////////////////////////////////////////////
    
    UIImageView *iconImgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 50, 120, 90)];
    iconImgView1.image = [UIImage imageNamed:@"face_icn.png"];
    iconImgView1.layer.cornerRadius = 8.0f;
    iconImgView1.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView1];
    
    UIImageView *iconImgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 180, 120, 90)];
    iconImgView2.image = [UIImage imageNamed:@"lips_icn.png"];
    iconImgView2.layer.cornerRadius = 8.0f;
    iconImgView2.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView2];
    
    UIImageView *iconImgView3 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 310, 120, 90)];
    iconImgView3.image = [UIImage imageNamed:@"eyes_icn.png"];
    iconImgView3.layer.cornerRadius = 8.0f;
    iconImgView3.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView3];
    
    UIImageView *iconImgView4 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 440, 120, 90)];
    iconImgView4.image = [UIImage imageNamed:@"nails_icn.png"];
    iconImgView4.layer.cornerRadius = 8.0f;
    iconImgView4.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView4];
    
    
    //////////////////////////////////////////////////////////////////////////
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
 //   btn1.backgroundColor = [UIColor greenColor];
    btn1.frame = CGRectMake(10, 15, 300, 110);
    btn1.tag = 41;
    [btn1 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn1];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
 //   btn2.backgroundColor = [UIColor cyanColor];
    btn2.frame = CGRectMake(10, 150, 300, 110);
    btn2.tag = 42;
    [btn2 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn2];
    
    UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
//    btn3.backgroundColor = [UIColor blueColor];
    btn3.frame = CGRectMake(10, 280, 300, 110);
    btn3.tag = 43;
    [btn3 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn3];
    
    UIButton *btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
 //   btn4.backgroundColor = [UIColor cyanColor];
    btn4.frame = CGRectMake(10, 410, 300, 110);
    btn4.tag =44;
    [btn2 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn4];
    
}

-(void)toGridView :(id)sender
{
    gridView * gView = [[self storyboard] instantiateViewControllerWithIdentifier:@"gridview"];
    [self.navigationController pushViewController:gView animated:YES];
    UIButton *someButton = (UIButton*)sender;
    NSString *str = [NSString stringWithFormat:@"%d", someButton.tag];
    gView.getString = str;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
