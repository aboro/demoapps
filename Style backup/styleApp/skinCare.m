//
//  skinCare.m
//  styleApp
//
//  Created by Mac Mini on 06/12/13.
//  Copyright (c) 2013 Mac Mini. All rights reserved.
//

#import "skinCare.h"

@interface skinCare ()

@end

@implementation skinCare

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = FALSE;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    scrlview.contentSize = CGSizeMake(320, 550);
    scrlview.showsVerticalScrollIndicator = FALSE;
    [scrlview setScrollEnabled:TRUE];
    scrlview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"skinCare_bg.png"]];
    
    UIImageView *rackImgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 130, 260, 60)];
    rackImgView1.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView1];
    
    UIImageView *rackImgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 280, 260, 60)];
    rackImgView2.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView2];
    
    UIImageView *rackImgView3 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 430, 260, 60)];
    rackImgView3.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView3];
    
    //////////////////////////////////////////////////////////////////////////
    
    UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(160, 90, 150, 20)];
    lbl1.text = @"Day";
    lbl1.textAlignment = NSTextAlignmentCenter;
    lbl1.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl1];
    
    UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(160, 240, 150, 20)];
    lbl2.text = @"Night";
    lbl2.textAlignment = NSTextAlignmentCenter;
    lbl2.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl2];
    
    UILabel *lbl3 = [[UILabel alloc] initWithFrame:CGRectMake(190, 370, 100, 45)];
    lbl3.numberOfLines = 2;
    lbl3.lineBreakMode = NSLineBreakByWordWrapping;
    lbl3.text = @"Special Treatment";
    lbl3.textAlignment = NSTextAlignmentCenter;
    lbl3.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl3];
    
    //////////////////////////////////////////////////////////////////////////
    
    UIImageView *iconImgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 50, 120, 90)];
    iconImgView1.backgroundColor = [UIColor lightGrayColor];
    iconImgView1.layer.cornerRadius = 8.0f;
    iconImgView1.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView1];
    
    UIImageView *iconImgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 200, 120, 90)];
    iconImgView2.backgroundColor = [UIColor lightGrayColor];
    iconImgView2.layer.cornerRadius = 8.0f;
    iconImgView2.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView2];
    
    UIImageView *iconImgView3 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 350, 120, 90)];
    iconImgView3.backgroundColor = [UIColor lightGrayColor];
    iconImgView2.layer.cornerRadius = 8.0f;
    iconImgView2.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView3];
    
    //////////////////////////////////////////////////////////////////////////
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
  //  btn1.backgroundColor = [UIColor greenColor];
    btn1.frame = CGRectMake(10, 10, 300, 120);
    btn1.tag = 21;
    [btn1 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn1];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
  //  btn2.backgroundColor = [UIColor cyanColor];
    btn2.frame = CGRectMake(10, 160, 300, 120);
    btn2.tag = 22;
    [btn2 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn2];
    
    UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
 //   btn3.backgroundColor = [UIColor blueColor];
    btn3.frame = CGRectMake(10, 310, 300, 120);
    btn3.tag = 23;
    [btn3 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn3];
}

-(void)toGridView :(id)sender
{
    gridView * gView = [[self storyboard] instantiateViewControllerWithIdentifier:@"gridview"];
    [self.navigationController pushViewController:gView animated:YES];
    UIButton *someButton = (UIButton*)sender;
    NSString *str = [NSString stringWithFormat:@"%d", someButton.tag];
    gView.getString = str;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
