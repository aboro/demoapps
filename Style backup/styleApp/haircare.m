//
//  makeUp.m
//  styleApp
//
//  Created by Mac Mini on 06/12/13.
//  Copyright (c) 2013 Mac Mini. All rights reserved.
//

#import "hairCare.h"

@interface hairCare ()



@end

@implementation hairCare

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
//this is a comment


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = FALSE;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"image"
                                                                    style:UIBarButtonItemStyleDone target:self action:@selector(popUp)];
    rightButton.image = [UIImage imageNamed:@"btn.png"];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    
    scrlview.contentSize = CGSizeMake(320, 550);
    scrlview.showsVerticalScrollIndicator = FALSE;
    [scrlview setScrollEnabled:TRUE];
    scrlview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"hairCare_bg.png"]];
    
    UIImageView *rackImgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 120, 260, 60)];
    rackImgView1.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView1];
    
    UIImageView *rackImgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 270, 260, 60)];
    rackImgView2.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView2];
    
    UIImageView *rackImgView3 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 420, 260, 60)];
    rackImgView3.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView3];
    
    //////////////////////////////////////////////////////////////////////////
    
    UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(190, 60, 100, 45)];
    lbl1.text = @"Clean & Condition";
    lbl1.numberOfLines = 2;
    lbl1.lineBreakMode = NSLineBreakByWordWrapping;
    lbl1.textAlignment = NSTextAlignmentCenter;
    lbl1.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl1];
    
    UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(160, 235, 130, 15)];
    lbl2.text = @"Colours";
    lbl2.textAlignment = NSTextAlignmentCenter;
    lbl2.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl2];
    
    UILabel *lbl3 = [[UILabel alloc] initWithFrame:CGRectMake(190, 360, 100, 45)];
    lbl3.numberOfLines = 2;
    lbl3.lineBreakMode = NSLineBreakByWordWrapping;
    lbl3.text = @"Special Treatment";
    lbl3.textAlignment = NSTextAlignmentCenter;
    lbl3.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl3];
    
    //////////////////////////////////////////////////////////////////////////
    
    UIImageView *iconImgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 40, 120, 90)];
    iconImgView1.backgroundColor = [UIColor lightGrayColor];
    iconImgView1.layer.cornerRadius = 8.0f;
    iconImgView1.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView1];
    
    UIImageView *iconImgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 190, 120, 90)];
    iconImgView2.backgroundColor = [UIColor lightGrayColor];
    iconImgView2.layer.cornerRadius = 8.0f;
    iconImgView2.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView2];
    
    UIImageView *iconImgView3 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 340, 120, 90)];
    iconImgView3.backgroundColor = [UIColor lightGrayColor];
    iconImgView3.layer.cornerRadius = 8.0f;
    iconImgView3.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView3];
    
    // *************************** uiButton Tag ******************************//
    
    
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
//    btn1.backgroundColor = [UIColor greenColor];
    btn1.frame = CGRectMake(10, 10, 300, 120);
    btn1.tag = 11;
    [btn1 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn1];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
  //  btn2.backgroundColor = [UIColor cyanColor];
    btn2.frame = CGRectMake(10, 160, 300, 120);
    btn2.tag = 12;
    [btn2 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn2];
    
    UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
 //   btn3.backgroundColor = [UIColor blueColor];
    btn3.frame = CGRectMake(10, 310, 300, 120);
    btn3.tag = 13;
    [btn3 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn3];
}

-(void)popUp
{
    UIActionSheet *ac = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Open Camera",@"Open Gallery", nil];
    [ac showInView:self.view];
}

-(void)toGridView :(id)sender
{
    gridView * gView = [[self storyboard] instantiateViewControllerWithIdentifier:@"gridview"];
    [self.navigationController pushViewController:gView animated:YES];
    UIButton *someButton = (UIButton*)sender;
    NSString *str = [NSString stringWithFormat:@"%d", someButton.tag];
    gView.getString = str;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
