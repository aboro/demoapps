//
//  gridView.h
//  styleApp
//
//  Created by Mac Mini on 20/12/13.
//  Copyright (c) 2013 Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface gridView : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    IBOutlet UICollectionView *gridViewPack;
}


@property(nonatomic, retain) NSString *getString;

@end
