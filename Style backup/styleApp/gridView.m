//
//  gridView.m
//  styleApp
//
//  Created by Mac Mini on 20/12/13.
//  Copyright (c) 2013 Mac Mini. All rights reserved.
//

#import "gridView.h"

@interface gridView ()

@end

@implementation gridView
@synthesize getString = _getString;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    gridViewPack.layer.backgroundColor = [[UIColor whiteColor]CGColor];
    NSLog(@"String value  is :- %@",self.getString);
}

#pragma mark UICollectionView Delegate Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 21; //[newliqourListArray count];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(2, 2, 2, 2);
}

#define kLabel 2

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"cell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *label = [[UIImageView alloc] initWithFrame:cell.bounds];
    label.layer.opacity = 0.7;
    label.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    label.layer.borderWidth = 1.0;
    //label.font = [UIFont systemFontOfSize:15];
    label.backgroundColor = [UIColor grayColor];
    //[newliqourListArray objectAtIndex:indexPath.row];
    [cell.contentView addSubview:label];
    
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
    //306, 171 is the width and height of uicollection resp. view perfect view
}


- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5; //return 8;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
