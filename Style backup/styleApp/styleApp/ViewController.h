//
//  ViewController.h
//  styleApp
//
//  Created by Mac Mini on 03/12/13.
//  Copyright (c) 2013 Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "hairCare.h"
#import "skinCare.h"
#import "bodyCare.h"
#import "makeUp.h"
#import "wishList.h"
#import "looksIlike.h"
@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UIScrollView *scrlview;
}
 

@end
