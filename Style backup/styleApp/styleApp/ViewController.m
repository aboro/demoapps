//
//  ViewController.m
//  styleApp
//
//  Created by Mac Mini on 03/12/13.
//  Copyright (c) 2013 Mac Mini. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	CGRect winSize = [[UIScreen mainScreen] bounds];
  
    self.navigationController.navigationBarHidden = TRUE;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    scrlview.contentSize = CGSizeMake(320, 750);
    scrlview.showsVerticalScrollIndicator = FALSE;
    [scrlview setScrollEnabled:TRUE];
    scrlview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"main_ground.png"]];
    // *************************** Single Sinle rack Images ********************************//
    
    UIImageView *rackImgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 110, 260, 60)];
    rackImgView1.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView1];
    
    UIImageView *rackImgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 230, 260, 60)];
    rackImgView2.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView2];
    
    UIImageView *rackImgView3 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 350, 260, 60)];
    rackImgView3.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView3];
    
    UIImageView *rackImgView4 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 470, 260, 60)];
    rackImgView4.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView4];
    
    UIImageView *rackImgView5 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 590, 260, 60)];
    rackImgView5.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView5];
    
    UIImageView *rackImgView6 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 710, 260, 60)];
    rackImgView6.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView6];
    
    
    // *************************** Single Sinle Lables ********************************//
    
    UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(200, 70, 100, 15)];
    lbl1.text = @"Hair Care";
    lbl1.textAlignment = NSTextAlignmentCenter;
    lbl1.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl1];
    
    UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(200, 190, 100, 15)];
    lbl2.text = @"Skin Care";
    lbl2.textAlignment = NSTextAlignmentCenter;
    lbl2.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl2];
    
    UILabel *lbl3 = [[UILabel alloc] initWithFrame:CGRectMake(200, 310, 100, 15)];
    lbl3.text = @"Body Care";
    lbl3.textAlignment = NSTextAlignmentCenter;
    lbl3.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl3];
    
    UILabel *lbl4 = [[UILabel alloc] initWithFrame:CGRectMake(200, 430, 100, 15)];
   // [lbl4 setFont:[UIFont fontWithName:@"Futura" size:12]];
    lbl4.text = @"Make Up";
    lbl4.textAlignment = NSTextAlignmentCenter;
    lbl4.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl4];
    
    UILabel *lbl5 = [[UILabel alloc] initWithFrame:CGRectMake(200, 550, 100, 15)];
    lbl5.text = @"Wish List";
    lbl5.textAlignment = NSTextAlignmentCenter;
    lbl5.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl5];
    
    UILabel *lbl6 = [[UILabel alloc] initWithFrame:CGRectMake(210, 650, 50, 45)];
    lbl6.numberOfLines = 2;
    lbl6.lineBreakMode = NSLineBreakByWordWrapping;
    lbl6.text = @"Looks I like";
    lbl6.textAlignment = NSTextAlignmentCenter;
    lbl6.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl6];
    
    
    
    // *************************** Single Sinle iconImage View ******************************//
    
    UIImageView *iconImgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 25, 120, 90)];
    iconImgView1.layer.cornerRadius = 8.0f;
    iconImgView1.layer.masksToBounds = YES;
    iconImgView1.image = [UIImage imageNamed:@"hairCare.png"];
    //
    [scrlview addSubview:iconImgView1];
    
    UIImageView *iconImgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 145, 120, 90)];
    iconImgView2.layer.cornerRadius = 8.0f;
    iconImgView2.layer.masksToBounds = YES;
    iconImgView2.image = [UIImage imageNamed:@"skinCare_icn.png"];
    
    [scrlview addSubview:iconImgView2];
    
    UIImageView *iconImgView3 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 265, 120, 90)];
    iconImgView3.image = [UIImage imageNamed:@"bodyCare_icn.png"];
    iconImgView3.layer.cornerRadius = 8.0f;
    iconImgView3.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView3];
    
    UIImageView *iconImgView4 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 385, 120, 90)];
    iconImgView4.image = [UIImage imageNamed:@"makeUp_icn.png"];
    iconImgView4.layer.cornerRadius = 8.0f;
    iconImgView4.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView4];
    
    UIImageView *iconImgView5 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 505, 120, 90)];
    iconImgView5.backgroundColor = [UIColor lightGrayColor];
    iconImgView5.layer.cornerRadius = 8.0f;
    iconImgView5.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView5];
    
    UIImageView *iconImgView6 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 625, 120, 90)];
    iconImgView6.backgroundColor = [UIColor lightGrayColor];
    iconImgView2.layer.cornerRadius = 8.0f;
    iconImgView2.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView6];
    
    // *************************** uiButton Tag ******************************//
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
  //  btn1.backgroundColor = [UIColor greenColor];
    btn1.frame = CGRectMake(10, 20, 300, 110);
    btn1.tag = 1;
    [btn1 addTarget:self action:@selector(hairCare) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn1];

    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
 //   btn2.backgroundColor = [UIColor cyanColor];
    btn2.frame = CGRectMake(10, 140, 300, 110);
    btn2.tag = 2;
    [btn2 addTarget:self action:@selector(skinCare) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn2];
    
    UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
 //   btn3.backgroundColor = [UIColor blueColor];
    btn3.frame = CGRectMake(10, 260, 300, 110);
    btn3.tag = 3;
    [btn3 addTarget:self action:@selector(bodyCare) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn3];
    
    
    UIButton *btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
 //   btn4.backgroundColor = [UIColor redColor];
    btn4.frame = CGRectMake(10, 380, 300, 110);
    btn4.tag = 4;
    [btn4 addTarget:self action:@selector(makeUp) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn4];
    
    UIButton *btn5 = [UIButton buttonWithType:UIButtonTypeCustom];
 //   btn5.backgroundColor = [UIColor yellowColor];
    btn5.frame = CGRectMake(10, 500, 300, 110);
    btn5.tag = 5;
    [btn5 addTarget:self action:@selector(wishList) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn5];
    
    UIButton *btn6 = [UIButton buttonWithType:UIButtonTypeCustom];
 //   btn6.backgroundColor = [UIColor purpleColor];
    btn6.frame = CGRectMake(10, 620, 300, 110);
    btn6.tag = 6;
    [btn6 addTarget:self action:@selector(looksILike) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn6];
}

-(void)hairCare
{
    hairCare *haircare = [[self storyboard] instantiateViewControllerWithIdentifier:@"haircare"];
    [self.navigationController pushViewController:haircare animated:YES];
}

-(void)skinCare
{
    skinCare *skincare = [[self storyboard] instantiateViewControllerWithIdentifier:@"skincare"];
    [self.navigationController pushViewController:skincare animated:YES];
}

-(void)bodyCare
{
    bodyCare *bodycare = [[self storyboard] instantiateViewControllerWithIdentifier:@"bodycare"];
    [self.navigationController pushViewController:bodycare animated:YES];
}

-(void)makeUp
{
    makeUp *makeup = [[self storyboard] instantiateViewControllerWithIdentifier:@"makeup"];
    [self.navigationController pushViewController:makeup animated:YES];
}

-(void)wishList
{
    wishList *wishlist = [[self storyboard] instantiateViewControllerWithIdentifier:@"wishlist"];
    [self.navigationController pushViewController:wishlist animated:YES];
}

-(void)looksILike
{
    looksIlike *looks = [[self storyboard] instantiateViewControllerWithIdentifier:@"looks"];
    [self.navigationController pushViewController:looks animated:YES];
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = TRUE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
