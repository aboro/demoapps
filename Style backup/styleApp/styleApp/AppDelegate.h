//
//  AppDelegate.h
//  styleApp
//
//  Created by Mac Mini on 03/12/13.
//  Copyright (c) 2013 Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
