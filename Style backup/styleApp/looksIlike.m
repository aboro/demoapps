//
//  looksIlike.m
//  styleApp
//
//  Created by Mac Mini on 06/12/13.
//  Copyright (c) 2013 Mac Mini. All rights reserved.
//

#import "looksIlike.h"

@interface looksIlike ()

@end

@implementation looksIlike

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationController.navigationBarHidden = FALSE;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    scrlview.contentSize = CGSizeMake(320, 750);
    scrlview.showsVerticalScrollIndicator = FALSE;
    [scrlview setScrollEnabled:TRUE];
    
    UIImageView *rackImgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 100, 260, 40)];
    rackImgView1.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView1];
    
    UIImageView *rackImgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 240, 260, 40)];
    rackImgView2.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView2];
    
    UIImageView *rackImgView3 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 370, 260, 40)];
    rackImgView3.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView3];
    
    UIImageView *rackImgView4 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 510, 260, 40)];
    rackImgView4.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView4];
    
    UIImageView *rackImgView5 = [[UIImageView alloc] initWithFrame:CGRectMake(30, 640, 260, 40)];
    rackImgView5.image = [UIImage imageNamed:@"rack.png"];
    [scrlview addSubview:rackImgView5];
    
    //////////////////////////////////////////////////////////////////////////
    
    UILabel *lbl1 = [[UILabel alloc] initWithFrame:CGRectMake(160, 70, 150, 20)];
    lbl1.text = @"Make Up";
    lbl1.textAlignment = NSTextAlignmentCenter;
    lbl1.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl1];
    
    UILabel *lbl2 = [[UILabel alloc] initWithFrame:CGRectMake(160, 210, 150, 20)];
    lbl2.text = @"Hair care";
    lbl2.textAlignment = NSTextAlignmentCenter;
    lbl2.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl2];
    
    UILabel *lbl3 = [[UILabel alloc] initWithFrame:CGRectMake(160, 340, 150, 20)];
    lbl3.text = @"Celebrities";
    lbl3.textAlignment = NSTextAlignmentCenter;
    lbl3.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl3];
    
    UILabel *lbl4 = [[UILabel alloc] initWithFrame:CGRectMake(160, 480, 150, 20)];
    lbl4.text = @"Hot Trends";
    lbl4.textAlignment = NSTextAlignmentCenter;
    lbl4.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl4];
    
    UILabel *lbl5 = [[UILabel alloc] initWithFrame:CGRectMake(160, 610, 150, 20)];
    lbl5.text = @"Classic";
    lbl5.textAlignment = NSTextAlignmentCenter;
    lbl5.backgroundColor = [UIColor clearColor];
    [scrlview addSubview:lbl5];
    
    //////////////////////////////////////////////////////////////////////////
    
    UIImageView *iconImgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 30, 120, 90)];
    iconImgView1.backgroundColor = [UIColor lightGrayColor];
    iconImgView1.layer.cornerRadius = 8.0f;
    iconImgView1.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView1];
    
    UIImageView *iconImgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 170, 120, 90)];
    iconImgView2.backgroundColor = [UIColor lightGrayColor];
    iconImgView2.layer.cornerRadius = 8.0f;
    iconImgView2.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView2];
    
    UIImageView *iconImgView3 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 300, 120, 90)];
    iconImgView3.backgroundColor = [UIColor lightGrayColor];
    iconImgView3.layer.cornerRadius = 8.0f;
    iconImgView3.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView3];
    
    UIImageView *iconImgView4 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 440, 120, 90)];
    iconImgView4.backgroundColor = [UIColor lightGrayColor];
    iconImgView4.layer.cornerRadius = 8.0f;
    iconImgView4.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView4];
    
    UIImageView *iconImgView5 = [[UIImageView alloc] initWithFrame:CGRectMake(70, 570, 120, 90)];
    iconImgView5.backgroundColor = [UIColor lightGrayColor];
    iconImgView5.layer.cornerRadius = 8.0f;
    iconImgView5.layer.masksToBounds = YES;
    [scrlview addSubview:iconImgView5];
    
    
    //////////////////////////////////////////////////////////////////////////
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.backgroundColor = [UIColor greenColor];
    btn1.frame = CGRectMake(10, 10, 300, 120);
    btn1.tag = 61;
    [btn1 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn1];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.backgroundColor = [UIColor cyanColor];
    btn2.frame = CGRectMake(10, 150, 300, 120);
    btn2.tag = 62;
    [btn2 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn2];
    
    UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn3.backgroundColor = [UIColor blueColor];
    btn3.frame = CGRectMake(10, 290, 300, 120);
    btn3.tag = 63;
    [btn3 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn3];
    
    UIButton *btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn4.backgroundColor = [UIColor cyanColor];
    btn4.frame = CGRectMake(10, 430, 300, 120);
    btn4.tag = 64;
    [btn2 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn4];
    
    UIButton *btn5 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn5.backgroundColor = [UIColor blueColor];
    btn5.frame = CGRectMake(10, 570, 300, 120);
    btn5.tag = 65;
    [btn3 addTarget:self action:@selector(toGridView:) forControlEvents:UIControlEventTouchUpInside];
    [scrlview addSubview:btn5];
}


-(void)toGridView :(id)sender
{
    gridView * gView = [[self storyboard] instantiateViewControllerWithIdentifier:@"gridview"];
    [self.navigationController pushViewController:gView animated:YES];
    UIButton *someButton = (UIButton*)sender;
    NSString *str = [NSString stringWithFormat:@"%d", someButton.tag];
    gView.getString = str;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
